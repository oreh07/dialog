local dia = require "dialogue"
local oth = require "other"

count = oth.getVariable("derpDialogue") + 1;
oth.setVariable("derpDialogue", count);

dia.say("derp" .. tostring(count));
dia.answer("derp","derped");

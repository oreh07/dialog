﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using System.Text.RegularExpressions;

public class Node : MonoBehaviour {
    public InputField inputText;
    public InputField nameText;
    public Button closeButton;
    public List<string> contacts;
    private LineRenderer line;
    private bool hided = false;
    private int timer = 0;
    private bool closeLevel = false;

    public List<string> getPointers() {
        List<string> result = new List<string>();

        string[] array = Regex.Split(inputText.text,".answer");
        for(int i = 1; i < array.Length; i++) {
            string[] strings = array[i].Split('"');
            if(strings.Length > 3 &&strings[3] != "_") {
                result.Add(strings[3]);
            }
        }
        array = Regex.Split(inputText.text, ".loadScript");
        for (int i = 1; i < array.Length; i++) {
            string[] strings = array[i].Split('"');
            if (strings.Length > 1 && strings[1] != "_") {
                result.Add(strings[1]);
            }
        }
        return result;
    }

    public string scriptName() {
        return nameText.text;
    }
    public void updateContacts()
    {
        //float aspect = Screen.width * 1f / Screen.height;
        //line.positionCount = contacts.Count*2;
        //line.startWidth = 0.01f;
        //line.endWidth = 0.01f;
        //for (int i = 0; i < contacts.Count; i++) {
        //    GameObject obj = GameObject.Find("node:" + contacts[i]);
        //    if (obj) {
        //        //Debug.Log(contacts[i]);
        //        Vector2 pos = new Vector2(transform.position.x / Screen.width * 2f * aspect - 1.95f, transform.position.y / Screen.height * 2f - 1.05f);
        //        line.SetPosition(i * 2, pos);
        //
        //        pos = new Vector2(obj.transform.position.x / Screen.width * 2f * aspect - 1.95f, obj.transform.position.y / Screen.height * 2f - 1.05f);
        //        line.SetPosition(i * 2 + 1, pos);
        //        //Debug.Log(this.name);
        //        //Debug.Log(obj.name);
        //    }
        //}


    }
    public void hide() {
        hided = !hided;
        RectTransform rect = this.GetComponent<RectTransform>();
        if (hided) {
            rect.sizeDelta = new Vector2(rect.rect.width-700, rect.rect.height - 1280);
        } else {
            rect.sizeDelta = new Vector2(rect.rect.width+700, rect.rect.height + 1280);
        }
        nameText.interactable = !hided;
        inputText.gameObject.SetActive(!hided);
    }
    public void close() {
        if (!closeLevel) {
            closeLevel = true;
            timer = 100;
            closeButton.image.color = new Color(1f, 0.3f, 0.3f, 0.7f);
        } else {
            closeButton.image.color = new Color(0.2f, 0.2f, 0.2f, 0.2f);
            closeLevel = false;

            Debug.Log("delete");
            Destroy(this.gameObject);
        }

    }
    void Awake() {
        line = GetComponent<LineRenderer>();
        nameText.text = "scriptName";
    }
	void Update () {
        if (closeLevel) {
            timer--;
            if(timer < 0) {
                closeButton.image.color = new Color(0.2f, 0.2f, 0.2f, 0.2f);
                closeLevel = false;
                timer = 0;
            }
        }

        for (int i = 0; i < contacts.Count; i++)
        {
            GameObject obj = GameObject.Find("node:" + contacts[i]);
            if (obj)
            {
                Debug.DrawLine(transform.position + new Vector3(10f, -10f, 0), obj.transform.position + new Vector3(10f, -10f, 0), Color.white);
            }
        }
    }
}

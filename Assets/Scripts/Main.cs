﻿using UnityEngine;
using UnityEngine.UI;
using UnityEditor;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Text.RegularExpressions;

public class Main : MonoBehaviour {
    public Canvas mainCanvas;
    public Transform draggable;
    public InputField unitName;
    private Camera cam;
    private Vector2 draggablePosition;
    private List<GameObject> nodeList;

    private const char betweenNodes = '\n';
    private const char beforeCoords = '\t';

    public void updateContacts() {
        for (int i = 0; i < draggable.childCount; i++) {
            Transform obj = draggable.GetChild(i);
            if (obj.name != "Panel") {
                obj.GetComponent<Node>().updateContacts();
            }
        }
    }
    public void updateDraggableBounds() {
        Vector2 offset = new Vector2(draggable.position.x, draggable.position.y) - draggablePosition;

        for (int i = 0; i < draggable.childCount; i++) {
            Transform obj = draggable.GetChild(i);
            if (obj.name != "Panel") {
                obj.position += new Vector3(offset.x, offset.y, 0);
            }
        }
        draggable.position = draggablePosition;
    }
    void Start() {
        cam = Camera.main;
        nodeList = new List<GameObject>();
        draggablePosition = draggable.position;
        unitName.text = "testUnit";
    }
    
    void Update()
    {
        Vector2 s = mainCanvas.GetComponent<RectTransform>().rect.size;
        cam.orthographicSize = s.y / 2;
        cam.transform.position = new Vector3(s.x / 2, s.y / 2, -10);
    }

    public void newDialogue() {
        for(int i = 0; i < nodeList.Count; i++) {
            DestroyImmediate(nodeList[i]);
        }
        nodeList = new List<GameObject>();

        Debug.Log("newDialogue");
    }
    private string getParentFolder(string path) {
        string folderPath = "";
        string[] array = path.Split('/');
        for (int i = 0; i < array.Length - 1; i++) {
            folderPath += array[i] + "/";
        }
        return folderPath;
    }
    public void openDialogue() {

        Debug.Log("openDialogue");

        string configFile = EditorUtility.OpenFilePanel("openfile", Application.dataPath + "Results", "dcfg");
        if (configFile.Length == 0)
            return;
        string folderPath = getParentFolder(configFile);
        string configText = System.IO.File.ReadAllText(configFile);
        //Debug.Log(folderPath);
        //Debug.Log(configFile);
        newDialogue();
        
        string[] nodes = configText.Split(betweenNodes);

        unitName.text = nodes[0];
        for (int i = 1; i < nodes.Length-1; i++) {
            string nodeName = nodes[i].Split(beforeCoords)[0];
            string position = nodes[i].Split(beforeCoords)[1];
            int xs = System.Convert.ToInt32(position.Split(',')[0]);
            int ys = System.Convert.ToInt32(position.Split(',')[1]);
            //Debug.Log(nodeName);
            //Debug.Log(xs);
            //Debug.Log(ys);

            GameObject gm = getNewNode();
            gm.transform.position = new Vector2(xs,ys);
            Node node = gm.GetComponent<Node>();
            node.nameText.text = nodeName;
            gm.name = "node:" + nodeName;

            string nodePath = folderPath + nodeName + ".txt"; // this.unitName.text + "_" + 
            if (System.IO.File.Exists(nodePath)) {
                string nodeText = System.IO.File.ReadAllText(nodePath);
                
                node.inputText.text = nodeText;
            }
        }
        updateNodes();
    }
    private GameObject getNewNode() {
        GameObject gm = (GameObject)Instantiate(Resources.Load("Prefabs/Node"), new Vector2(Screen.currentResolution.width / 4, Screen.currentResolution.height / 4), new Quaternion(0f, 0f, 0f, 0f));
        gm.GetComponent<Node>().hide();
        gm.name = "node:" + gm.GetComponent<Node>().nameText.text;
        gm.GetComponent<Node>().inputText.onEndEdit.AddListener(delegate { onNodeEdited(gm); });
        gm.GetComponent<Node>().nameText.onEndEdit.AddListener( delegate { onNodeEdited(gm); });
        gm.GetComponent<Node>().inputText.text = "local l = require \"libs\"\n\nl.say(\"\");\n\nl.answer(\"\", \"\");\nl.answer(\"\", \"\");";
        

        gm.transform.SetParent(draggable);
        nodeList.Add(gm);
        return gm;
    }
    public void newNode() {
        getNewNode();
    }
    public void onNodeEdited(GameObject gm) {
        Debug.Log(gm);
        gm.name = "node:" + gm.GetComponent<Node>().nameText.text;
        updateNodes();
    }
    public void save() {

        string configPath = "F:/Downloads/Folder/Unity/Road/Assets/lua/testCity/dialogue/";
        if (!System.IO.File.Exists(configPath)) {
            configPath = EditorUtility.SaveFilePanel("savefile", configPath, this.unitName.text, "dcfg");
            //Debug.Log(configPath);
            if(configPath.Length == 0) {
                return;
            }
        }
        string configFolder = getParentFolder(configPath);

        string text = unitName.text + betweenNodes;
        for (int i = 0; i < nodeList.Count; i++) {
            if (nodeList[i] == null)
                continue;
            string nodeName = nodeList[i].GetComponent<Node>().scriptName();
            text += nodeList[i].GetComponent<Node>().scriptName() + beforeCoords + System.Convert.ToString(System.Convert.ToInt32(nodeList[i].transform.position.x)) + "," + System.Convert.ToString(System.Convert.ToInt32(nodeList[i].transform.position.y)) + "" + betweenNodes;

            string url = configFolder + nodeName + ".txt"; // this.unitName.text + "_" + 

            System.IO.File.WriteAllText(url, nodeList[i].GetComponent<Node>().inputText.text);
        }
        System.IO.File.WriteAllText(configPath, text);
    }
    public void updateNodes() {

        for (int i = 0; i < nodeList.Count; i++) {
            List<string> strings = nodeList[i].GetComponent<Node>().getPointers();
            //Debug.Log(nodeList[i].transform.name);
            //for (int j = 0; j < strings.Count; j++) {
            //    Debug.Log("--" + strings[j]);
            //}

            nodeList[i].GetComponent<Node>().contacts = strings;
            nodeList[i].GetComponent<Node>().updateContacts();
        }

    }
}

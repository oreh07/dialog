﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UnityEngine.EventSystems;

public class Draggable : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler {
    public static GameObject itemBeingDragged;
    public bool notification;
    Vector3 startPosition;
    Transform startParent;
    protected Vector2 offset;

    public virtual void onDrag() { }

    public virtual void OnBeginDrag(PointerEventData eventData) {
        onDrag();
        itemBeingDragged = gameObject;
        startPosition = transform.position;
        startParent = transform.parent;
        offset = new Vector2(transform.position.x, transform.position.y) - eventData.position;
    }
    public virtual void OnDrag(PointerEventData eventData) {
        transform.position = eventData.position + offset;
        if (notification) {
            Camera.main.GetComponent<Main>().updateContacts();
        }
    }
    public virtual void OnEndDrag(PointerEventData eventData) {
        itemBeingDragged = null;
        if (notification) {
            Camera.main.GetComponent<Main>().updateDraggableBounds();
            Camera.main.GetComponent<Main>().updateContacts();
        }
    }
}

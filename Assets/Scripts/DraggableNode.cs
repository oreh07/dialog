﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UnityEngine.EventSystems;

public class DraggableNode : Draggable {
    public override void OnDrag(PointerEventData eventData) {
        transform.position = eventData.position + offset;
        if (notification) {
            this.GetComponent<Node>().updateContacts();
        }
    }

    public override void onDrag() {
        this.transform.SetAsLastSibling();
    }
}
